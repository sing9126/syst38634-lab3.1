package password;

/*
 * Name:Dilraj Singh
 * Student Number: 991547818
 
 * Class will host the methods which will fulfill the following requirements
 1.	A password must have at least 8 characters
 2.	A password must contain at least two digits
 
 The class will be created using TDD.
 * */

public class PasswordValidate {

	private static int MIN_LENGTH = 8;

	public static boolean isValidLength(String password) {

		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		else {
		return false;
		}
	}

	public static boolean hasEnoughDigits(String password) {
		
		int count=0;
		char c;
		for(int i=0; i < password.length(); i++) {
			c = password.charAt(i);
			if(!Character.isLetterOrDigit(c)) {
				return false;
			
			}
			if(Character.isDigit(c)) {
				count++;
			}
		}
		System.out.println(count);
		if(count < 2) {
			return false;
		}
		else {
		return true;
	}}
}
