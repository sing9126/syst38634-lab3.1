package passwordTest;

import static org.junit.Assert.*;

import org.junit.Test;

import password.PasswordValidate;

/*
 * Name:Dilraj Singh
 * Student Number: 991547818
 
 * This Test class will verify if the passwords entered will fulfill the following requirements
 1.	A password must have at least 8 characters
 2.	A password must contain at least two digits
 
 The class will be created using TDD.
 * */

public class PasswordValidateTest {

	@Test
	public void testIsValidLengthRegular() {
		// fail("invalid Length for password!");
		assertTrue("invalid length", PasswordValidate.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("invalid length", PasswordValidate.isValidLength(null));
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("invalid length", PasswordValidate.isValidLength("        "));
	}

	@Test
	public void testIsValidLengthExceptionBoundaryIn() {

		assertTrue("invalid length", PasswordValidate.isValidLength("Password"));
	}
	
	@Test
	public void testIsValidLengthExceptionBoundaryOut() {

		assertFalse("invalid length", PasswordValidate.isValidLength("Passwrd"));
	}
	
	
	@Test
	public void testHasEnoughDigitsRegular() {
		//fail("Less than two digits");
		assertTrue("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8or4"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		//fail("Less than two digits");
		assertFalse("Less than two digits", PasswordValidate.hasEnoughDigits("Pass-1or4"));
	}
	
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		//fail("Less than two digits");
		assertTrue("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8or46"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		//fail("Less than two digits");
		assertFalse("Less than two digits", PasswordValidate.hasEnoughDigits("Pass8ord"));
	}
	
	
	
	
	

	/*
	 * @Test public void testIsValidLengthRegular() {
	 * //fail("invalid Length for password!"); assertTrue("invalid length",
	 * PasswordValidate.isValidLength("1234567890")); }
	 * 
	 * @Test public void testIsValidLengthException() {
	 * 
	 * assertFalse("invalid length", PasswordValidate.isValidLength("")); }
	 * 
	 * 
	 * @Test public void testIsValidLengthExceptionNull() {
	 * 
	 * assertFalse("invalid length", PasswordValidate.isValidLength(null)); }
	 * 
	 * @Test public void testIsValidLengthExceptionSpaces() { //10 Spaces
	 * assertFalse("invalid length", PasswordValidate.isValidLength("          "));
	 * }
	 * 
	 * @Test public void testIsValidLengthExceptionBoundaryIn() {
	 * 
	 * assertTrue("invalid length", PasswordValidate.isValidLength("12345678")); }
	 * 
	 * @Test public void testIsValidLengthExceptionBoundaryOut() {
	 * 
	 * assertFalse("invalid length", PasswordValidate.isValidLength("1234567")); }
	 */
}
