package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:55");
		//fail("Not yet implemented");
		assertTrue("Wrong result", totalMilliseconds == 55);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0T");
		//fail("Invalid Milliseconds");
		assertTrue("Wrong result", totalMilliseconds == 55);
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invalid Milliseconds", totalMilliseconds == 999);	
}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:1000");
		assertFalse ("Number of milliseconds exceeds the range", totalMilliseconds == 1000);	
}

}
